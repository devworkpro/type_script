import '../global'; // Object.assign

import * as types from '../types';
import { MessageDescriptor, messages } from './messages';

export type IdType = number | string;

export interface ObjectRef {
  clsType: LscmTypes;
  id: IdType;
  name: string;
  sn?: string;
}

export enum LscmTypes {
  CTL = 'ctl',
  Group = 'group',
  Region = 'region',

  Site = 'site',
  Segment = 'segment',
  CR = 'cr',
  Order = 'order',
}

export class ObjectBase<T> {
  constructor(jsonData?: Partial<T>) {
    if (jsonData) Object.assign(this, jsonData);
  }

  /**
   * Class type of this kind of objects
   */
  public static clsType: LscmTypes;

  /**
   * Class name/title of this kind of objects
   */
  public static clsName: MessageDescriptor;

  /**
   * Class type of this kind of objects
   */
  get clsType() {
    return (this.constructor as typeof ObjectBase).clsType;
  }

  /**
   * Class name/title of this kind of objects
   */
  get clsName() {
    return (this.constructor as typeof ObjectBase).clsName;
  }

  public id?: IdType; // id is invalid if being creating new object.
  public name?: string;
  public sn?: string;
  public desc?: string;

  /** CAUTION: _refs is a non-persistent property of objects. */
  private _refs?: ObjectRef[];

  /** Groups that this object belongs to */
  get groups(): ObjectRef[] {
    return this._refs
      ? this._refs.filter((ref) => ref.clsType === LscmTypes.Group)
      : [];
  }

  protected toRef(id?: IdType): ObjectRef | undefined {
    return this._refs && id
      ? this._refs.find((ref) => ref.id === id)
      : undefined;
  }
  protected toId(ref?: ObjectRef): IdType | undefined {
    if (ref) {
      if (!this._refs) this._refs = [];
      const idx = this._refs.findIndex((item) => item.id === ref.id);
      idx < 0 ? this._refs.push(ref) : (this._refs[idx] = ref);
      return ref.id;
    }
    return undefined;
  }

  protected toRefArray(idArray: IdType[] | undefined): ObjectRef[];
  protected toRefArray<U extends { [x in F]: IdType }, F extends string>(
    idArray: U[] | undefined,
    field: F,
  ): Array<{ [x in keyof U]: x extends F ? ObjectRef : U[x] }>;

  protected toRefArray(idArray: any[] | undefined, field?: string) {
    const refArray: any[] = [];
    if (field) {
      for (const item of idArray || []) {
        const ref = this.toRef(item[field]);
        if (ref) refArray.push({ ...item, [field]: ref });
      }
      return refArray;
    }

    for (const id of idArray || []) {
      const ref = this.toRef(id);
      if (ref) refArray.push(ref);
    }
    return refArray;
  }

  protected toIdArray(refArray: ObjectRef[]): IdType[];
  protected toIdArray<U extends { [x in F]: ObjectRef }, F extends string>(
    refArray: U[],
    field: F,
  ): Array<{ [x in keyof U]: x extends F ? IdType : U[x] }>;

  protected toIdArray(refArray: any[], field?: string) {
    if (field) {
      return refArray.map((item: any) => ({
        ...item,
        [field]: this.toId(item[field]),
      }));
    }
    return refArray.map((ref: ObjectRef) => this.toId(ref));
  }
}

/**
 * 货物时效 / CTL (Cargo Time Limit)
 */
export class CTL extends ObjectBase<CTL> {
  public static clsType = LscmTypes.CTL;
  public static clsName = messages.ctl;

  public tlDays?: number;
  public tlHour?: types.TimeStr;

  get tlHour2() {
    return types.timeToMoment(this.tlHour);
  }

  set tlHour2(tlHour: types.TimeMoment | undefined) {
    this.tlHour = types.timeToStr(tlHour, 'HH:mm');
  }
}

/**
 * 分组 / Group
 */
export class Group extends ObjectBase<Group> {
  public static clsType = LscmTypes.Group;
  public static clsName = messages.group;

  public type?: LscmTypes;
  private members?: IdType[];

  get members2() {
    return this.toRefArray(this.members);
  }
  set members2(array) {
    this.members = this.toIdArray(array);
  }
}

/**
 * 区域 / Region
 */
export class Region extends ObjectBase<Region> {
  public static clsType = LscmTypes.Region;
  public static clsName = messages.region;

  private subs?: IdType[];

  get subs2() {
    return this.toRefArray(this.subs);
  }
  set subs2(array) {
    this.subs = this.toIdArray(array);
  }
}

/**
 * 站点 / Site
 */
export class Site extends ObjectBase<Site> {
  public static clsType = LscmTypes.Site;
  public static clsName = messages.site;

  private superSite?: IdType;
  private region?: IdType;
  public lat?: number; // Latitude
  public long?: number; // Longitude

  /** Operation time periods */
  public opTime?: Array<[types.TimeStr, types.TimeStr]>;
  public pickupTime?: types.TimeStr[];
  public deliveryTime?: types.TimeStr[];

  /** List of CTLs which the site can handle. */
  private ctl?: IdType[];

  /** Process capability (per day) */
  public procCap?: types.ValueUnitPair<number, types.ProcessUnitKeys>;

  /** Operation speed of handling CTLs */
  private ctlOpSpd?: Array<{
    ctl: IdType;
    unload: number; // Unload speed, its unit is from "unloadUnit" property
    proc: number; // Process/transport speed, its unit is from "procUnit" property
    load: number; // Load speed, its unit is from "loadUnit" property
  }>;

  public unloadUnit?: types.ProcessSpeedUnitKeys;
  public procUnit?: types.ProcessSpeedUnitKeys;
  public loadUnit?: types.ProcessSpeedUnitKeys;

  public fixedCost?: types.MoneyRatePair;

  public include?: boolean;

  get superSite2() {
    return this.toRef(this.superSite);
  }
  set superSite2(superSite) {
    this.superSite = this.toId(superSite);
  }

  get region2() {
    return this.toRef(this.region);
  }
  set region2(region) {
    this.region = this.toId(region);
  }

  get ctl2() {
    return this.toRefArray(this.ctl);
  }
  set ctl2(array) {
    this.ctl = this.toIdArray(array);
  }

  get ctlOpSpd2() {
    return this.toRefArray(this.ctlOpSpd, 'ctl');
  }
  set ctlOpSpd2(array) {
    this.ctlOpSpd = this.toIdArray(array, 'ctl');
  }
}

/**
 * 线段 / Segment
 */
export class Segment extends ObjectBase<Segment> {
  public static clsType = LscmTypes.Segment;
  public static clsName = messages.segment;

  /** From site */
  public siteA?: IdType;
  /** To site */
  public siteB?: IdType;
  /** Distance */
  public dist?: types.DistancePair;
  /** Duration */
  public dur?: types.TimePair;

  get siteA2() {
    return this.toRef(this.siteA);
  }
  set siteA2(siteA) {
    this.siteA = this.toId(siteA);
  }

  get siteB2() {
    return this.toRef(this.siteB);
  }
  set siteB2(siteB) {
    this.siteB = this.toId(siteB);
  }
}

/**
 * 货物路径/路由 / CR (CargoRoute)
 */
export class CR extends ObjectBase<CR> {
  public static clsType = LscmTypes.CR;
  public static clsName = messages.cargoRoute;

  /** Origin site */
  private orig?: IdType;
  /** Destination site */
  private dest?: IdType;
  /** Other stop sites except origin and destination */
  private stops?: IdType[];

  /** List of CTLs that will go this route. */
  private ctl?: IdType[];

  /** Probability, range: [0.0, 1.0] */
  public prob?: number;
  /** Priority, integer */
  public pri?: number;

  /** Weekday of service */
  public svcWeek?: types.ServiceWeek;
  public effectDate?: types.DateStr;
  public expiryDate?: types.DateStr;

  public include?: boolean;

  get orig2() {
    return this.toRef(this.orig);
  }
  set orig2(orig) {
    this.orig = this.toId(orig);
  }

  get dest2() {
    return this.toRef(this.dest);
  }
  set dest2(dest) {
    this.dest = this.toId(dest);
  }

  get stops2() {
    return this.toRefArray(this.stops);
  }
  set stops2(array) {
    this.stops = this.toIdArray(array);
  }

  get ctl2() {
    return this.toRefArray(this.ctl);
  }
  set ctl2(array) {
    this.ctl = this.toIdArray(array);
  }
}

/**
 * 货物订单 / Order
 */
export class Order extends ObjectBase<Order> {
  public static clsType = LscmTypes.Order;
  public static clsName = messages.order;

  /** Origin site */
  private orig?: IdType;
  /** Destination site */
  private dest?: IdType;
  /** CTL */
  private ctl?: IdType;

  /** Not used yet */
  // private product?: IdType;

  public pickupDate?: types.DateStr;
  public pickupTime?: types.TimeStr;

  /** Quantity */
  public qty?: number;
  public wgt?: types.WeightPair;
  public vol: types.VolumePair;

  public include?: boolean;

  get orig2() {
    return this.toRef(this.orig);
  }
  set orig2(orig) {
    this.orig = this.toId(orig);
  }

  get dest2() {
    return this.toRef(this.dest);
  }
  set dest2(dest) {
    this.dest = this.toId(dest);
  }

  get ctl2() {
    return this.toRef(this.ctl);
  }
  set ctl2(ctl) {
    this.ctl = this.toId(ctl);
  }
}
